﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model.UI;
using Tekla.Structures.Plugins;
using TSM = Tekla.Structures.Model;

namespace FB.TrussJoint3_1
{
    [Plugin("TrussJoint3.1")]
    [PluginUserInterface("FB.TrussJoint3_1.MainWindow")]
    public class Plugin : PluginBase
    {
        private readonly PluginData data;

        public TSM.Model Model { get; } = new TSM.Model();

        public Plugin(PluginData data)
        {
            this.data = data;
        }

        public override List<InputDefinition> DefineInput()
        {
            Picker picker = new Picker();
            List<InputDefinition> input_definition = new List<InputDefinition>();
            
            //TODO: Определить выбор опорной геометрии

            return input_definition;
        }

        public override bool Run(List<InputDefinition> Input)
        {
            try
            {
                if (Input.Count > 0)
                {
                    //TODO: Определить алгоритм верхнего уровня абстракции
                }
            }
            catch (Exception ex)
            {
            }
            return true;
        }
    }
}
